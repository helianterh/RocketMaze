import { Component, EventEmitter, ViewChild } from '@angular/core';
import {CdkDrag,CdkDragHandle} from '@angular/cdk/drag-drop';
import { RouterOutlet } from '@angular/router';
import RocketConfigJson from '../model/RocketConfig.json'; 
import { RocketConfig } from '../model/RocketConfig';
import { Maze } from '../model/Maze';
import { GameState } from '../model/GameState';
import { GameComponent } from './game/game.component';
import { Evaluator } from '../model/Evaluator';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CdkDrag, CdkDragHandle, GameComponent], 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'rocket-maze';

  rocketConfig : RocketConfig = Object.assign(new RocketConfig(), RocketConfigJson); 
  gameState : GameState;



  constructor(){
    this.gameState = new GameState(this.rocketConfig, new Maze(this.rocketConfig, this.rocketConfig.levels[0]));
    console.log(this.gameState);
  }
  


  launch(){
    this.gameState.launch();
  }

  reset(){
    this.gameState.reset();
  }

  evaluate(){
    let evaluator = new Evaluator(this.gameState);

    let vector = [-1,-1,-1,-1,-1,-1,-1,-1,-1];
    let vectors : number[][] = [];
    evaluator.evaluateRecursive(vector, vectors);
    console.log(vectors);

    console.log(evaluator.evaluateCurrentGameState(null));    
  }
}
