import { Component, ElementRef, Input } from '@angular/core';
import { MazeCell } from '../../model/MazeCell';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-cell',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cell.component.html',
  styleUrl: './cell.component.scss'
})
export class CellComponent {
  @Input() mazeCell! : MazeCell;

  constructor(private el: ElementRef){
    
  }

  ngAfterViewInit(){
    this.mazeCell.initGetPosition(() => [this.el.nativeElement.offsetLeft + this.el.nativeElement.offsetWidth / 2, this.el.nativeElement.offsetTop + this.el.nativeElement.offsetHeight / 2]);
  }
}
