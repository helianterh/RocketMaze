import { Component, ElementRef, Input, ViewChild, ViewChildren } from '@angular/core';
import { GameState } from '../../model/GameState';
import { MazeComponent } from '../maze/maze.component';
import { RocketComponent } from '../rocket/rocket.component';
import { CommonModule } from '@angular/common';
import { AnimationBuilder, AnimationPlayer, animate, style } from '@angular/animations';

@Component({
  selector: 'app-game',
  standalone: true,
  imports: [MazeComponent,RocketComponent,CommonModule],
  templateUrl: './game.component.html',
  styleUrl: './game.component.scss'
})
export class GameComponent {
  @Input() gameState! : GameState;

  animnationCompleteCallback : (() => void) | null = null;

  constructor(private animationBuilder: AnimationBuilder){
    
  }

  ngAfterViewInit(){
    this.gameState.animationStart = this.animationStart.bind(this);
    this.gameState.initAnimationCompleteCallback = this.initAnimationCompleteCallback.bind(this);
  }

  initAnimationCompleteCallback(callback : () => void){
    this.animnationCompleteCallback = callback;
  }

  animationStart(gs : GameState){

    let element = document.getElementById('rocket');
    if(element){
    /*
    if(rocket){
      rocket.style.left = gs.getRocketLeft() + 'px';
      rocket.style.top = gs.getRocketTop() + 'px';
    }
    */


    
    if(this.animnationCompleteCallback)
      this.moveAnimation(element, gs.getRocketLeft(), gs.getRocketTop(), () => {
      if(element){
          element.style.left = gs.getRocketLeft() + 'px';
          element.style.top = gs.getRocketTop() + 'px';
      }
        this.animnationCompleteCallback!();
    });

      /*
    setTimeout(() => {
      if(this.animnationCompleteCallback)
      this.animnationCompleteCallback();
    }, 200);*/
    }
  }


  moveAnimation(element: any, x : number, y : number, callBack : () => void) {
 
    const builder = this.animationBuilder.build([
      style({ left : element.offsetLeft, top : element.offsetTop}),
      animate('0.175s', style({ left : x + 'px', top  : y + 'px' }))
    ]);

    // use the returned factory object to create a player
    const player = builder.create(element);
    
    player.onDone(() => {
      player.destroy();
      callBack();
    });
 
    player.play();
  }
}
