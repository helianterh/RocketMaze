import { Component, Input, Renderer2, ViewChild } from '@angular/core';
import { PanelComponent } from '../panel/panel.component';
import { Maze } from '../../model/Maze';
import { CommonModule } from '@angular/common';
import { CellComponent } from '../cell/cell.component';
import { AnimationBuilder, AnimationPlayer, animate, style } from '@angular/animations';
import { SelectionEvent } from '../../model/SelectionEvent';

@Component({
  selector: 'app-maze',
  standalone: true,
  imports: [PanelComponent,CellComponent,CommonModule],
  templateUrl: './maze.component.html',
  styleUrl: './maze.component.scss'
})
export class MazeComponent {

  @Input() maze! : Maze;

  currentDraggingPanel : any;
  currentSwitchingPanel : any;
  currentDraggingOffset : any;

  moveAnimations : AnimationPlayer[] = [];

  constructor(private animationBuilder: AnimationBuilder, private readonly renderer: Renderer2){}

  ngAfterViewInit(){
    this.maze.initPositions();
  }

  onPanelDrag(e : SelectionEvent){
    
   
    if (this.currentDraggingPanel != null) {
      this.currentDraggingPanel.style.left = (e.clientX + this.currentDraggingOffset[0]) + 'px';
      this.currentDraggingPanel.style.top  = (e.clientY + this.currentDraggingOffset[1]) + 'px';
    }

    let elements = document.elementsFromPoint(e.clientX, e.clientY);

    //Determine the panel container of the source
    let target : any = e.target;
    let panelContainerSource = this.getPanelContainer(target.parentElement);

    let panelElement : any = null;

    //Determine if there is a panel container where on the current dragging
    let panelContainer = elements.find(e => e.className == "panel-container");
    if(panelContainer){
      let panelElement : any = panelContainer.children[0];

      //Check if the panel container differ from the current switching
      if(this.currentSwitchingPanel != panelElement)
      {
        let lastSwitchingPanel = this.currentSwitchingPanel;

        if(panelContainerSource != panelContainer){
          this.currentSwitchingPanel = panelElement;
          this.initElementMove(panelElement);
          this.moveAnimation(panelElement, panelContainerSource.offsetLeft, panelContainerSource.offsetTop, () => {
            panelElement.style.left = panelContainerSource.offsetLeft + 'px';;
            panelElement.style.top = panelContainerSource.offsetTop + 'px';;
          });
        }
        else
          this.currentSwitchingPanel = null;

        if(lastSwitchingPanel){
          let lastSwitchingPanelContainer = this.getPanelContainer(lastSwitchingPanel);
          let player = this.moveAnimation(lastSwitchingPanel, lastSwitchingPanelContainer.offsetLeft, lastSwitchingPanelContainer.offsetTop, () => {
            this.resetElementMove(lastSwitchingPanel);
          });
        }
      }
    
    }
  }


  onPanelDrop(e : SelectionEvent){
    
    if(this.currentDraggingPanel && this.currentSwitchingPanel && this.currentDraggingPanel != this.currentSwitchingPanel){

      let currentDraggingPanelContainer = this.getPanelContainer(this.currentDraggingPanel);
      let currentSwitchingPanelContainer = this.getPanelContainer(this.currentSwitchingPanel);

      this.renderer.removeChild(currentDraggingPanelContainer, this.currentDraggingPanel);
      this.renderer.removeChild(currentSwitchingPanelContainer, this.currentSwitchingPanel);

      this.renderer.appendChild(currentDraggingPanelContainer, this.currentSwitchingPanel);
      this.renderer.appendChild(currentSwitchingPanelContainer, this.currentDraggingPanel);

      this.maze.movePanelById(parseInt(currentDraggingPanelContainer.getAttribute("mazepanelid")), 
                              parseInt(currentSwitchingPanelContainer.getAttribute("mazepanelid")));

    }

    if(this.currentDraggingPanel){

      let panel = this.currentDraggingPanel;
      this.currentDraggingPanel = null;

      let panelContainerSource = this.getPanelContainer(panel);

      if(panelContainerSource){
        let player = this.moveAnimation(panel, panelContainerSource.offsetLeft, panelContainerSource.offsetTop, () => {
          this.resetElementMove(panel);
        });
      }
    }

    if(this.currentSwitchingPanel){
      let panel = this.currentSwitchingPanel;
      this.currentSwitchingPanel = null;

      let panelContainerSource = this.getPanelContainer(panel);

      if(panelContainerSource){
        let player = this.moveAnimation(panel, panelContainerSource.offsetLeft, panelContainerSource.offsetTop, () => {
          this.resetElementMove(panel);
        });
      }
    }


  }

  onPanelStartDragDrop(e : SelectionEvent){

    let target : any = e.target;

    //Determine the panel
    let panel = target.parentElement;
    while(panel && panel.localName != "app-panel")
      panel = panel.parentElement;

    this.currentDraggingPanel = panel;
    this.currentDraggingOffset = [ panel.offsetLeft - e.clientX, panel.offsetTop - e.clientY ];

    this.initElementMove(panel);
    this.initElementDragging(panel);
  }

  moveAnimation(element: any, x : number, y : number, callBack : () => void) : AnimationPlayer {
 
    const builder = this.animationBuilder.build([
      style({ left : element.offsetLeft, top : element.offsetTop}),
      animate('0.8s ease', style({ left : x, top  : y }))
    ]);

    // use the returned factory object to create a player
    const player = builder.create(element);
    
    player.onDone(() => {
      player.destroy();
      callBack();
      this.moveAnimations.splice(this.moveAnimations.indexOf(player), 1);
    });
 
    this.moveAnimations.push(player);
    player.play();

    return player;
  }

  initElementMove(element : any){
    element.style.width = element.offsetWidth + 'px';
    element.style.height = element.offsetHeight + 'px';
    element.style.position = "absolute"; 
    element.style.zIndex = 50;
  }

  initElementDragging(element : any){
    element.style.zIndex = 100;
  }

  resetElementMove(element : any){
    element.style.left = '';
    element.style.top  = '';
    element.style.width =  '';
    element.style.height =  '';
    element.style.position = ''; 
    element.style.zIndex = 10;
  }

  getPanelContainer(element : any) : any{
    let panelContainer= element.parentElement;
    while(panelContainer && panelContainer.className != "panel-container")
      panelContainer = panelContainer.parentElement;
    return panelContainer;
  }
}
