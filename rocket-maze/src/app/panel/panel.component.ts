import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import {MatGridListModule} from '@angular/material/grid-list';
import { CellComponent } from '../cell/cell.component';
import { MazePanel } from '../../model/MazePanel';
import { CommonModule } from '@angular/common';
import { DragDropPanelState } from '../../model/DragDropPanelState';
import { AnimationBuilder, animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionEvent } from '../../model/SelectionEvent';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [MatGridListModule,CellComponent,CommonModule],
  templateUrl: './panel.component.html',
  styleUrl: './panel.component.scss',

})
export class PanelComponent {

  @Input() mazePanel! : MazePanel;
  @Output() onPanelDrop = new EventEmitter<SelectionEvent>();
  @Output() onPanelDrag = new EventEmitter<SelectionEvent>();
  @Output() onPanelStartDragDrop = new EventEmitter<SelectionEvent>();

  constructor(private el: ElementRef, private animationBuilder: AnimationBuilder){
    
  }

  onMouseDown(e : MouseEvent) {
    e.preventDefault();
    this.onPanelStartDragDrop.emit(new SelectionEvent(e.clientX, e.clientY, e.target));
  } 
  
  onMouseUp(e : MouseEvent){
    e.preventDefault();
    this.onPanelDrop.emit(new SelectionEvent(e.clientX, e.clientY, e.target));
  } 
  
  onMouseMove(e : MouseEvent){
    e.preventDefault();
    this.onPanelDrag.emit(new SelectionEvent(e.clientX, e.clientY, e.target));
  }

  onTouchStart(e : TouchEvent){
    e.preventDefault();
    if(e.touches.length > 0)
      this.onPanelStartDragDrop.emit(new SelectionEvent(e.touches[0].clientX, e.touches[0].clientY, e.touches[0].target));
  }

  onTouchMove(e : TouchEvent){
    e.preventDefault();
    if(e.touches.length > 0)
      this.onPanelDrag.emit(new SelectionEvent(e.touches[0].clientX, e.touches[0].clientY, e.touches[0].target));
  }

  onTouchEnd(e : TouchEvent){
    if(e.touches.length > 0)
      this.onPanelDrop.emit(new SelectionEvent(e.touches[0].clientX, e.touches[0].clientY, e.touches[0].target));
    else
      this.onPanelDrop.emit(new SelectionEvent(0, 0, null));
  }


}
