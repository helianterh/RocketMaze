import { GameState } from "./GameState";

export class EvaluatorResult{

    star_acquired : number = 0;
    success :  boolean = false;
    completed :  boolean = false;
    exploded : boolean = false;
    infinite_loop : boolean = false;
    lastChar : string = '';

    update(gameState : GameState){

        let cell = gameState.maze.getMazeCell(gameState.rocket_row, gameState.rocket_col);

        this.star_acquired = gameState.star_acquired;
        this.exploded = gameState.exploded;
        this.success = cell.currentChar == gameState.rocketConfig.end_char;
        this.completed = this.success && gameState.star_acquired == gameState.maze.star_count;
        this.infinite_loop  = gameState.move_count > 1000;
        this.lastChar = cell.currentChar;
    }

    isDone(){
        return this.success || this.exploded || this.infinite_loop;
    }
    
}