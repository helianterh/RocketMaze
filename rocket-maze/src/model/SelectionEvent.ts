 export class SelectionEvent{
    clientX : number;
    clientY : number;
    target : EventTarget | null;
    constructor(clientX : number, clientY : number, target : EventTarget | null){
        this.clientX = clientX;
        this.clientY = clientY;
        this.target = target;
    }
}