import { LevelConfig } from "./LevelConfig";
import { MazeCell } from "./MazeCell";
import { MazePanel } from "./MazePanel";
import { RocketConfig } from "./RocketConfig";

export class Maze {
    level_id: number;
    
    //[row][column]
    panels : MazePanel[][];
    initialPanels : MazePanel[][];

    wall_top : MazeCell[];
    wall_right : MazeCell[];
    wall_bottom : MazeCell[];
    wall_left : MazeCell[];

    star_count : number;
    start_maze_row : number;
    start_maze_col : number;
    end_maze_row : number;
    end_maze_col : number;
    maze_max_row : number;
    maze_max_col : number;
    mapLeft: number[][];
    mapTop: number[][];

    constructor(rocketConfig : RocketConfig , levelConfig : LevelConfig){
        this.level_id = levelConfig.id;
        this.star_count = 0;
        this.start_maze_row = -1;
        this.start_maze_col = -1;
        this.end_maze_row = -1;
        this.end_maze_col = -1;
        this.mapLeft = [];
        this.mapTop = [];

        //Initialize the panels
        this.panels = [];
        this.initialPanels = [];
        const max_row_panel = 3;
        const max_col_panel = 3;
        for(let row = 0; row < max_row_panel; row++){
            this.panels.push([]);
            this.initialPanels.push([]);
            for(let col = 0; col < max_col_panel; col++){
                var panel = new MazePanel();
                this.panels[row].push(panel);
                this.initialPanels[row].push(panel);
            }
        }

        //Determine the size of the maze
        this.maze_max_row = levelConfig.rows.length;
        this.maze_max_col = levelConfig.rows[0].replaceAll(rocketConfig.split_char, "").length;

        //Initialize the cells of the panel
        for(let row = 1; row < this.maze_max_row - 1; row++){
            let row_str : string = levelConfig.rows[row];

            //Retirer les murs
            row_str = row_str.substring(1, row_str.length - 1);

            //Compter les étoiles
            this.star_count += (row_str.match(new RegExp(rocketConfig.star_char.replace("*","\\*"), "g")) || []).length;

            let cell_row = (row - 1) % 3;
            let panel_row = Math.floor((row - 1) / 3);
            let row_str_split = row_str.split(rocketConfig.split_char);

            for(let panel_col = 0; panel_col < row_str_split.length; panel_col++){
                for(let cell_col = 0; cell_col < row_str_split[panel_col].length; cell_col++){
                    if(this.panels[panel_row][panel_col].cells[cell_row] == undefined){
                        this.panels[panel_row][panel_col].cells[cell_row] = [];
                    }

                    this.panels[panel_row][panel_col].cells[cell_row][cell_col] = new MazeCell(row_str_split[panel_col].charAt(cell_col));
                }
            }
        }


        //Initialize the walls
        this.wall_top = [];
        this.wall_right = [];
        this.wall_bottom = [];
        this.wall_left = [];

        //Initialize the cells of the top wall
        let index = 0;
        for(let col = 0; col < levelConfig.rows[0].length; col++){
            let char = levelConfig.rows[0].charAt(col);
            if(char == rocketConfig.split_char){
                continue;
            }
            
            if(char == rocketConfig.start_char){
                this.start_maze_row = 0;
                this.start_maze_col = index;
            }
            else if(char == rocketConfig.end_char){
                this.end_maze_row = 0;
                this.end_maze_col = index;
            }

            this.wall_top.push(new MazeCell(char));
            index++;
        }

        //Initialize the cells of the down wall
        index = 0;
        for(let col = 0; col < levelConfig.rows[levelConfig.rows.length - 1].length; col++){
            let char = levelConfig.rows[levelConfig.rows.length - 1].charAt(col);
            if(char == rocketConfig.split_char){
                continue;
            }

            if(char == rocketConfig.start_char){
                this.start_maze_row = levelConfig.rows.length - 1;
                this.start_maze_col = index;
            }
            else if(char == rocketConfig.end_char){
                this.end_maze_row = levelConfig.rows.length - 1;
                this.end_maze_col = index;
            }

            this.wall_bottom.push(new MazeCell(char));
            index++;
        }

        //Initialize the cells of the right wall
        for(let row = 0; row < levelConfig.rows.length; row++){
            let char = levelConfig.rows[row].charAt(levelConfig.rows[row].length - 1);
            if(char == rocketConfig.start_char){
                this.start_maze_row = row;
                this.start_maze_col = levelConfig.rows[row].length - 1;
            }
            else if(char == rocketConfig.end_char){
                this.end_maze_row = row;
                this.end_maze_col = levelConfig.rows[row].length - 1;
            }

            this.wall_right.push(new MazeCell(char));
        }

        //Initialize the cells of the left wall
        for(let row = 0; row < levelConfig.rows.length; row++){
            let char = levelConfig.rows[row].charAt(0);
            if(char == rocketConfig.start_char){
                this.start_maze_row = row;
                this.start_maze_col = 0;
            }
            else if(char == rocketConfig.end_char){
                this.end_maze_row = row;
                this.end_maze_col = 0;
            }

            this.wall_left.push(new MazeCell(char));
        }

    }

    movePanelById(panel_id_src : number, panel_id_dst : number) {

        let panel_id = 0;
        let panel_row_src : number | undefined = undefined;
        let panel_col_src : number | undefined = undefined;
        let panel_row_dst : number | undefined = undefined;
        let panel_col_dst : number | undefined = undefined;
        
        for(let row = 0; row < this.panels.length; row++){
            for(let col = 0; col < this.panels[row].length; col++){
                if(panel_id == panel_id_src){
                    panel_row_src = row;
                    panel_col_src = col;
                }
                else if(panel_id == panel_id_dst){
                    panel_row_dst = row;
                    panel_col_dst = col;
                }
                panel_id++;
            }
        }

        if(panel_row_src != undefined && panel_col_src != undefined && panel_row_dst != undefined && panel_col_dst != undefined){
            let panel_src = this.panels[panel_row_src][panel_col_src];
            this.panels[panel_row_src][panel_col_src] = this.panels[panel_row_dst][panel_col_dst];
            this.panels[panel_row_dst][panel_col_dst] = panel_src;
        }



        /*
        let dst : MazePanel = this.panels[row_dst][col_dst];
        this.panels[row_dst][col_dst] = this.panels[row_src][col_src];
        this.panels[row_src][col_src] = dst;   
        */
    }

    getMazeCell(mazeRow : number, mazeCol : number){

        //Determine if it is the top wall
        if(mazeRow == 0)
            return this.wall_top[mazeCol];

        //Determine if it is the bottom awall
        if(mazeRow == this.maze_max_row - 1)
            return this.wall_bottom[mazeCol];

        //Determine if it is the left wall
        if(mazeCol == 0)
            return this.wall_left[mazeRow];

        //Determine if it is the right wall
        if(mazeCol == this.maze_max_col - 1)
            return this.wall_right[mazeRow];

        let panelRow = this.getPanelRow(mazeRow);
        let panelCol = this.getPanelCol(mazeCol);
        let cellRow = this.getCellRow(mazeRow);
        let cellCol = this.getCellCol(mazeCol);
        return this.panels[panelRow][panelCol].cells[cellRow][cellCol];
    }

    getPanelRow(mazeRow : number) : number {
        return Math.floor((mazeRow - 1) / this.panels.length);
    }

    getPanelCol(mazeCol : number) : number {
        return Math.floor((mazeCol - 1) / this.panels[0].length);
    }

    getCellRow(mazeRow : number) : number {
        return (mazeRow - 1) % this.panels[0][0].cells.length;
    }

    getCellCol(mazeCol : number) : number {
        return (mazeCol - 1) % this.panels[0][0].cells[0].length;
    }

    getPanelByInitialPanelId(panelId : number) : MazePanel | null{
        let currentPanelId = 0;
        for(let row = 0; row < this.initialPanels.length; row++){
            for(let col = 0; col < this.initialPanels[row].length; col++){
                if(panelId == currentPanelId)
                    return this.initialPanels[row][col];
                else
                    currentPanelId++;
            }
        }

        return null;
    }

    reset(){
        for(let row = 0; row < this.maze_max_row; row++ ){
            for(let col = 0; col < this.maze_max_col; col++){
                let cell = this.getMazeCell(row, col);
                cell.currentChar = cell.configChar;
            }
        }
    }

    getInitialPanels() : MazePanel[] {
        let arr = [];
        for(let row = 0; row < this.initialPanels.length; row++ ){
            for(let col = 0; col < this.initialPanels[row].length; col++){
                arr.push(this.initialPanels[row][col]);
            }
        }
        return arr;
    }

    getWallSections(wall : MazeCell[]) : MazeCell[][]{

        let sections : MazeCell[][] = [];
        let wall_index = 1;
        for(let section_index = 0; section_index < this.panels.length; section_index++){
            let section : MazeCell[] = [];
            
            for(let cell_index = 0; cell_index < this.panels[0][0].cells.length; cell_index++)
                section.push(wall[wall_index++]);

            sections.push(section);
        }

        return sections;
    }

    initPositions(){

        for(let row = 0; row < this.maze_max_row; row++ ){
            this.mapLeft[row] = [];
            this.mapTop[row] = [];
            for(let col = 0; col < this.maze_max_col; col++){
                let cell = this.getMazeCell(row, col);
                let position = cell.getPosition();
                this.mapLeft[row].push(position[0]);
                this.mapTop[row].push(position[1]);
            }
        }

    }

    print(){
     
        for(let row = 0; row < this.maze_max_row; row++ ){
            let rowStr = "";
            for(let col = 0; col < this.maze_max_col; col++){
                rowStr += this.getMazeCell(row, col).currentChar;
            }
            console.log(rowStr);
        }

    }

}