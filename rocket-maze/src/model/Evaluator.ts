import { EvaluatorResult } from "./EvaluatorResult";
import { GameState } from "./GameState";
import { MazePanel } from "./MazePanel";

export class Evaluator {

    gameState: GameState;

    constructor(gameState : GameState){
        this.gameState = gameState;
    }

    evaluateAll(){

        let panels = this.gameState.maze.getInitialPanels();
        let panelCount = panels.length;

        let id_available : number[] = [];
        let vector : number[] = [];

        for(let i = 0; i < panelCount; i++){
            vector.push(i);
        }

        let vector_index = vector.length - 1;
        while(vector.length < panelCount || (vector[0] != (vector.length  - 1) && vector[vector.length - 1] != 0))
        {
            //Remplir le vecteur
            if(vector.length < panelCount){
                let next_id = Math.max(...id_available);
            }

            //vector_index = 7
            //012345678 ()
            //01234567  (8)
            //01234568  (7)
            //012345687 ()
            //vector_index = 6
            //0123456   (7,8)
            //0123457   (6,8) --switch
            //01234576  (8)
            //012345768 ()
            //01234576  (8)
            //01234578  (6)


        }

    }


    evaluateCurrentGameState(vector : number[] | null) : EvaluatorResult{
        this.gameState.reset();
        if(vector)
            this.replacePanels(vector);
        let result = new EvaluatorResult();
        while(!result.isDone()){
            this.gameState.move();
            result.update(this.gameState);
        }
        return result;
    }

    replacePanels(vector : number[]){
        let panelId = 0;
        for(let row = 0; row < this.gameState.maze.panels.length; row++){
            for(let col = 0; col < this.gameState.maze.panels[row].length; col++){

                let vectorPanel = this.gameState.maze.getPanelByInitialPanelId(vector[panelId]);
                if(vectorPanel)
                    this.gameState.maze.panels[row][col] = vectorPanel;

                panelId++;
            }
        }
    }




    evaluateRecursive(vector : number[], vectors : number[][]){

        if(vector[vector.length - 1] != -1){
            vectors.push(vector);
            vector
            return;
        }

        //Determine the next index to fill up
        for(let index = 0; index < vector.length; index++){
            if(vector[index] == -1){
                //Determine the next number to fill
                for(let panel_id = 0; panel_id < vector.length; panel_id++){
                    if(vector.indexOf(panel_id) == -1){
                        vector[index] = panel_id;
                        this.evaluateRecursive(vector, vectors);
                    }
                }
                
            }
        }
    }
}