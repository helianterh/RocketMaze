import { EventEmitter } from "@angular/core";
import { Maze } from "./Maze";
import { MazeCell } from "./MazeCell";
import { RocketConfig } from "./RocketConfig";
import { Subscription } from "rxjs";

export class GameState{
    rocketConfig : RocketConfig;
    maze : Maze;
    rocket_row : number;
    rocket_col : number;
    rocket_vector_row : number;
    rocket_vector_col : number;
    rocket_cell : MazeCell | undefined;
    exploded : boolean;
    ended : boolean;
    star_acquired : number;
    move_count : number;

    animationStart : ((gs : GameState) => void) | null = null;
    initAnimationCompleteCallback : ((cb : () => void) => void) | null = null;

    constructor(rocketConfig : RocketConfig, maze : Maze){

        this.rocketConfig = rocketConfig;
        this.maze = maze;
        this.rocket_row = 0;
        this.rocket_col = 0;
        this.rocket_vector_row = 0;
        this.rocket_vector_col = 0;
        this.exploded = false;
        this.ended = false;
        this.star_acquired = 0;
        this.move_count = 0;
        this.reset();
    }

    reset(){

        //Reset the position of the rocket
        this.rocket_row = this.maze.start_maze_row;
        this.rocket_col= this.maze.start_maze_col;

        //Reset the move vector
        this.rocket_vector_row = 0;
        this.rocket_vector_col = 0;
        if(this.rocket_row == 0)
            this.rocket_vector_row = 1;
        else if(this.rocket_row == this.maze.maze_max_row - 1)
            this.rocket_vector_row = -1;
        else if(this.rocket_col == 0)
            this.rocket_vector_col = 1;
        else if(this.rocket_col== this.maze.maze_max_col - 1)
            this.rocket_vector_col = -1;

        //Reset the star acquired count
        this.star_acquired = 0;
        this.exploded = false;
        this.ended = false;

        this.move_count = 0;

        //Reset the state of the maze
        this.maze.reset();
    }

    launch(){
        this.reset();

        let isOver = false;
        let recursive : Function;
        recursive = () => {
            this.waitAnimation(() => {
                this.move();
                if(!isOver)
                    recursive();
                isOver = this.ended;
            });
        };
        recursive();
    }

    waitAnimation(callBack : () => void){

        if(this.initAnimationCompleteCallback)
            this.initAnimationCompleteCallback(callBack);

        if(this.animationStart)
            this.animationStart(this);
    }

    move(){
        
        this.rocket_row += this.rocket_vector_row;
        this.rocket_col += this.rocket_vector_col;
        this.rocket_cell = this.maze.getMazeCell(this.rocket_row, this.rocket_col);
        const current_char = this.rocket_cell.currentChar;
        if(current_char == this.rocketConfig.wall_char || 
           current_char == this.rocketConfig.block_char || 
           current_char == this.rocketConfig.start_char){
            this.exploded = true;
            this.ended = true;
            this.rocket_vector_row = 0;
            this.rocket_vector_col = 0;
           }
        else if(current_char == this.rocketConfig.star_char){
            this.star_acquired++;
            this.rocket_cell.currentChar = this.rocketConfig.star_acquired_char;
        }
        else if(current_char == this.rocketConfig.up_char){
            this.rocket_vector_row = -1;
            this.rocket_vector_col = 0;
        }
        else if(current_char == this.rocketConfig.right_char){
            this.rocket_vector_row = 0;
            this.rocket_vector_col = 1;
        }
        else if(current_char == this.rocketConfig.down_char){
            this.rocket_vector_row = 1;
            this.rocket_vector_col = 0;
        }
        else if(current_char == this.rocketConfig.left_char){
            this.rocket_vector_row = 0;
            this.rocket_vector_col = -1;
        }
        else if(current_char == this.rocketConfig.end_char){
            this.ended = true;
            this.rocket_vector_row = 0;
            this.rocket_vector_col = 0;
        }

        this.move_count++;
    }

    print(){
     
        for(let row = 0; row < this.maze.maze_max_row; row++ ){
            let rowStr = "";
            for(let col = 0; col < this.maze.maze_max_col; col++){

                if(row == this.rocket_row && col == this.rocket_col)
                    rowStr += "0";
                else
                    rowStr += this.maze.getMazeCell(row, col).currentChar;
            }
            console.log(rowStr);
        }

    }


    getRocketTop(){
        if(this.maze.mapTop.length > 0)
            return this.maze.mapTop[this.rocket_row][this.rocket_col];
        else
            return 0;
    }

    getRocketLeft(){
        if(this.maze.mapLeft.length > 0)
            return this.maze.mapLeft[this.rocket_row][this.rocket_col];
        else
            return 0;
    }
}