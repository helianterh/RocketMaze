import { EventEmitter } from "@angular/core";

export class MazeCell {

    configChar : string;
    currentChar : string;
    getPosition : () => number[];

    constructor(config_char : string){
        this.configChar = config_char;
        this.currentChar = config_char;
        this.getPosition = () => [0, 0];
    }

    initGetPosition(getPosition : () => number[]){
        this.getPosition = getPosition;
    }
}