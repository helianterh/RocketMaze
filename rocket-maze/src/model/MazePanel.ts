import { MazeCell } from "./MazeCell";

export class MazePanel {

    //[row][column] 
    cells : MazeCell[][];

    constructor(){
        this.cells = [];
    }

    toArray() : MazeCell[] {
        let arr = [];
        for(let row = 0; row < this.cells.length; row++ ){
            for(let col = 0; col < this.cells[row].length; col++){
                arr.push(this.cells[row][col]);
            }
        }
        return arr;
    }

}