import { LevelConfig } from "./LevelConfig";
export class RocketConfig {

    version : string;
    wall_char : string;
    split_char : string;
    empty_char : string;
    up_char : string;
    right_char : string;
    down_char : string;
    left_char : string;
    block_char : string; 
    star_char : string;
    star_acquired_char : string;
    start_char : string;
    end_char : string;
    levels : LevelConfig[];

    constructor(){

            this.version  = "";
            this.wall_char = "";
            this.split_char = "";
            this.empty_char = "";
            this.up_char = "";
            this.right_char = "";
            this.down_char = "";
            this.left_char = "";
            this.block_char = ""; 
            this.star_char = "";
            this.star_acquired_char = "";
            this.start_char = "";
            this.end_char = "";
            this.levels = [];
        }


}