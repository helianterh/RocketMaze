export class LevelConfig{
    id : number;
    rows: string[];

    constructor(){
        this.id = -1;
        this.rows = [];
    }
}